apt update

DEBIAN_FRONTEND=noninteractive TZ=Asia/Bangkok apt install -y tzdata

apt install -y sudo

sudo apt install -y nodejs npm

sudo apt install -y python3 python3-pip

sudo python3 -m pip install jupyterhub

sudo npm install -g configurable-http-proxy

python3 -m pip install jupyterlab notebook