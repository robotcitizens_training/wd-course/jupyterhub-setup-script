# jupyterhub-setup-script

## Installation guide
```
./jupyterhub_server_setup.bash
```

## Create user
```
./create_user.bash {USERNAME}
```

## Run server
```
./running.bash
```